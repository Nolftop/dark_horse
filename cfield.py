#! /usr/bin/env python3

from random import randrange
import tkinter as tk


class Field:
    def __init__(self, width, height, pawns_count):
        self.width, self.height = width, height
        self.knigth_alive = True
        self.pawns_count = pawns_count
        self.bias = 3
        self.EMPTY = '.'
        self.PAWN = 'p'
        self.KNIGHT = 'K'
        self.field = ([[self.EMPTY] * (self.width + self.bias * 2)
                      for _ in range(self.height + self.bias * 2)])
        self.pawnsonfield = 0
        self.score = 0
        self.knight_xy = (randrange(self.width), randrange(self.height))
        self.put_knight(*self.knight_xy)
        self.random_put_pawns()
        self.moves_count = 0
        self.buttonList = []

    def get(self, x, y):
        return self.field[y + self.bias - 1][x + self.bias-1]

    def _set(self, value, x, y):
        self.get(x, y)
        self.field[y + self.bias - 1][x + self.bias-1] = value

    def refresh(self):
        gap = '   '
        for x in range(self.width):
            print('  '+str(x + 1), end=' ')
        print()
        for y in range(self.height):
            linefield = str(y + 1) + ' '
            for x in range(self.width):
                linefield += (self.get(x, y) + gap)
            print(linefield)
            print()

    def random_put_pawns(self):
        while self.pawnsonfield < self.pawns_count:
            self.pawn_xy = (randrange(self.width), randrange(self.height))
            if (self.get(*self.pawn_xy) and self.get(self.pawn_xy[0]-1, self.pawn_xy[1]+1) and self.get(self.pawn_xy[0]-1, self.pawn_xy[1]-1)) == self.EMPTY:
                self._set(self.PAWN, *self.pawn_xy)
                self.pawnsonfield += 1

    def put_knight(self, x, y):
        self._set(self.EMPTY, *self.knight_xy)
        if self.get(x, y) == self.PAWN:
            self.score += 1
            self.pawns_count -= 1
            self.pawnsonfield -=1
        self._set(self.KNIGHT, x, y)
        self.knight_xy = (x, y)

    def pawns_attack(self):
        for y in range(len(self.field)):
            if self.get(self.knight_xy[0]+1, self.knight_xy[1]+1) == self.PAWN:
                self._set(self.PAWN, *self.knight_xy)
                self._set(self.EMPTY, self.knight_xy[0]+1, self.knight_xy[1]+1)
                self.knigth_alive = False
            if self.get(self.knight_xy[0]+1, self.knight_xy[1]-1) == self.PAWN:
                self._set(self.PAWN, *self.knight_xy)
                self._set(self.EMPTY, self.knight_xy[0]+1, self.knight_xy[1]-1)
                self.knigth_alive = False

    def move_horse(self, x, y):
        mlst = [[-1, -2], [-2, -1], [-2, 1], [-1, 2], [1, -2], [2, -1], [2, 1], [1, 2]]
        dist = [self.knight_xy[0] - x, self.knight_xy[1] - y]
        if dist in mlst:
            self.put_knight(x, y)
            self.moves_count += 1
            self.pawns_attack()
            self.refresh()
            for y in range(self.height):
                self.buttonList.append([])
                for x in range(self.width):
                    self.buttonList[y][x].config(text=self.get(x, y))


class Game:
    def __init__(self, field):
        self.game = 1
        self.field = field
        self.win = False
        self.defeat = False

    def win(self):
        print(".    .")
        print(r"|\/\/|")
        print("|____|")
        print('Score: ' + str(self.field.score))

    def defeat(self):
        print("Game over!")
        print('Score: ' + str(self.field.score))

    def check_score(self):
        print('Score: '+str(self.field.score), 'Moves: '+str(self.field.moves_count))
        if not self.field.pawns_count:
            self.win = True
        if not self.field.knigth_alive:
            self.defeat = True

class Frame:
    def __init__(self, field, play):
        self.play = play
        self.field = field
        self.clicks = 0
        self.window = tk.Tk()
        self.score_label = tk.Label(self.window , text='Score \n'+str(self.field.score))
        self.moves_label = tk.Label(self.window , text='Moves \n'+str(self.field.moves_count))
        self.pawns_left_label = tk.Label(self.window , text='Pawns left \n'+str(self.field.pawnsonfield))

    def create_window(self):
        for y in range(self.field.height):
            self.field.buttonList.append([])
            for x in range(self.field.width):
                color = 'orange' if (y + x) % 2 else 'yellow'
                self.field.buttonList[y].append(tk.Button(self.window,
                                                        text=self.field.get(x,y),
                                                        bg=color, height=2, width=2, command=lambda y=y,
                                                        x=x, self=self: self.commands_in_one_button_handler(x, y)))
                self.field.buttonList[y][x].grid(row=y, column=x)
            self.score_label.grid(row=1, column=self.field.width+1)
            self.moves_label.grid(row=2, column=self.field.width+1)
            self.pawns_left_label.grid(row=3, column=self.field.width+1)
        self.window.mainloop()

    def commands_in_one_button_handler(self, x, y):
        self.field.move_horse(x, y)
        self.count_stat()
        self.refresh_window(x, y)
        self.game_final()

    def refresh_window(self, y, x):
        for y in range(self.field.height):
            for x in range(self.field.width):
                    self.field.buttonList[y][x].config(text=self.field.get(x, y))

    def count_stat(self):
        self.clicks += 1
        self.score_label.config(text='Score \n'+str(self.field.score))
        self.moves_label.config(text='Moves \n'+str(self.field.moves_count))
        self.pawns_left_label.config(text='Pawns left \n'+str(self.field.pawnsonfield))

    def game_final(self):
        self.play.check_score()
        if self.play.defeat or self.play.win:
            for y in range(self.field.height):
                for x in range(self.field.width):
                    self.field.buttonList[y][x].config(state='disabled')
            self.dialog = tk.Tk()
            final = ' win' if self.play.win else ' lose'
            message = tk.Label(self.dialog, text='You' + final +'!')
            exit = tk.Button(self.dialog, text='Exit', command = self.quit_handler)
            restart = tk.Button(self.dialog, text='Restart')
            message.grid(row=1, column=2)
            exit.grid(row=3, column=4)
            restart.grid(row=3, column=6)
            self.dialog.mainloop()

    def quit_handler(self):
        self.dialog.destroy()
        self.window.destroy()

    def restart_handler(self):








def main(width=8, height=8, knigth_alive=True, pawns=3):
    field = Field(width, height, pawns)
    field.refresh()
    play = Game(field)
    frame = Frame(field, play)
    frame.create_window()


if __name__ == '__main__':
    main()
